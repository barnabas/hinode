# Hinode

This is an Alba vNext demo/prototype.

## Development
Prerequisites: 
- [Node.js](https://nodejs.org/en/)
- [Yarn](https://yarnpkg.com/en/)
- [Zeit Now](https://zeit.co/now) (to deploy)

The sub-projects under the main monorepo are:
  * [/api](./api): API microservice
  * [/auth](./auth): Authentication/authorization microservice
  * [/mobile](./mobile): end-user mobile browser UI
  * [/shared](./shared): shared UI code
  * [/www](./www): management desktop browser UI

To run a local server for development, do a combination of the following:

1. Open a terminal in the "api" directory and start the dev server
    - `yarn run dev`
    - http://localhost:3000/
1. Open a terminal in the "auth" directory and start the dev server
    - `yarn run dev`
    - http://localhost:3100/
1. Open a terminal in the "www" directory and start the dev server
    - `yarn run serve`
    - http://localhost:8080/
1. Open a terminal in the "mobile" directory and start the dev server
    - `yarn run serve`
    - http://localhost:8181/

The www and mobile dev servers will proxy any `/api` or `/auth` requests to the API and Auth microservices, respectively.
At a command line in any of the above directories, enter `yarn run` to see a list of available commands.

## Deployment
At the root directory of this project, run `now`. That's it.

## Tech Stack
Here are links to the tools that are used in this project.

### Client/Mobile
* [Vue.js](https://vuejs.org/) - JavaScript framework
* [Vue CLI](https://cli.vuejs.org/) - Vue tooling for development and building
* [Vue Router](https://router.vuejs.org/) - client-side routing
* [VueX](https://vuex.vuejs.org/) - state management pattern/library
* [Vuetify](https://vuetifyjs.com/en/) - Material Design UI framework for Vue
* [Jest](https://jestjs.io/en/) - unit test runner

### API
* [Micro](https://github.com/zeit/micro) + [Micro-dev](https://github.com/zeit/micro-dev):
  extremely lightweight web server for microservices

### Hosting
* [Zeit Now](https://zeit.co/now) - serverless hosting
* [Maptiler](https://www.maptiler.com/) - maps