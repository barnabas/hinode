// test data generator
const fs = require("fs");
const gb = require("geojson-bounds");
const uuid = require("uuid");
const utils = require("./utils");

const FIRST_NAMES = "Akiko Akira Ayako Daisuke Etsuko Emi Fred Hana Haruki Hiro Hiroshi Hirotada Ichiro Jimmy Jiro Jun Junichi Junko Kazuko Keiko Ken Kimiko Kyoko Maki Mari Mariko Mei Naoko Naomi Onigiri Reiko Ryo Sakura Shouta Taichi Takako Takasuke Takuan Taro Tetsuo Tetsuya Yasu Yasutaka Yasuo Yoko Yoshitaka".split(
  " "
);
const LAST_NAMES = "Abe Hara Hashimoto Hayashi Hiraishi Ikeda Inoue Ishii Ishibashi Ishikawa Ito Jones Kato Kimura Kina Kobayashi Matsumoto Mori Nakagawa Nakamura Saito Sasaki Sato Shimizu Shiokawa Suzuki Takahashi Tanaka Tanase Teraoka Tashiro Watanabe Yamada Yamaguchi Yamaimo Yamamoto Yamazaki Yasuda Yoshida".split(
  " "
);
const STREET_NAMES = "Alder Ames Arroyo Briarwood Carlson Colorado Cowper Elsinore Embarcadero Evergreen Grove Harvard Juniper Kellogg Kipling Manchester Mariposa Maybell Mesa Newell Pitman Ramona Sandpiper Tennyson Tulip Ventura Whitclem Wisteria Yale".split(
  " "
);
const STREET_TYPES = ["St", "Dr", "Pl", "Way", "Cir"];
const POSTAL_CODES = ["94025", "94026", "94027"];
const ADDRESS_STATUSES = [
  "new",
  "new",
  "new",
  "new",
  "valid",
  "valid",
  "valid",
  "valid",
  "duplicate",
  "moved",
  "dnc",
  "notValid",
  "notValid"
];
const USERS = [
  "caleb",
  "sophia",
  "elsa",
  "roberto",
  "lisa",
  "abraham",
  "isaac",
  "jacob"
];

const NOTES = [
  "Gate",
  "Big dog",
  "No soliciting sign",
  "test note 1",
  "test note 2"
];

const { territories } = require("../data/territories.json");
const { features } = require("../data/boundaries.json");
const addresses = [];
const territoryHistory = [];
const addressHistory = [];

// -----------------------  begin main

territories.forEach(territory => {
  const { id, name } = territory;
  let historyDate = utils.randomDate();

  for (let th = 0; th < 5; th++) {
    historyDate = utils.randomDate(historyDate);
    territoryHistory.push({
      id: uuid(),
      territoryId: id,
      status: th % 2 === 0 ? "started" : "finished",
      userId: utils.takeRandom(USERS),
      createdOn: historyDate
    });
  }

  const feature = features.find(f => f.properties.id === id);
  if (!feature) return;
  const extent = gb.extent(feature);
  const city = name.substring(0, name.lastIndexOf(" "));

  const count = utils.randomInt(30, 50);
  for (let a = 0; a < count; a++) {
    const address = makeAddress(extent, { city, territoryId: id });
    let addressDate = utils.randomDate();
    for (let ah = 0; ah < 5; ah++) {
      addressDate = utils.randomDate(addressDate, 7);
      addressHistory.push({
        id: uuid(),
        addressId: address.id,
        status: ah === 0 && address.status !== "new" ? "contacted" : "notHome",
        note: Math.utils.random() > 0.8 ? utils.takeRandom(NOTES) : null,
        createdOn: addressDate
      });
    }
    addresses.push(address);
  }
});

writeJSON("addresses.json", { addresses });
writeJSON("addressHistory.json", { history: addressHistory });
writeJSON("territoryHistory.json", { history: territoryHistory });

// -----------------------  end main

function makeAddress(extent, values) {
  const houseNum = Math.floor(utils.random(100, 9999));
  const [lng1, lat1, lng2, lat2] = extent;
  return {
    id: uuid(),
    status: utils.takeRandom(ADDRESS_STATUSES),
    name: `${utils.takeRandom(FIRST_NAMES)} ${utils.takeRandom(LAST_NAMES)}`,
    phoneNumber: makePhoneNumber(),
    streetAddress: `${houseNum} ${utils.takeRandom(
      STREET_NAMES
    )} ${utils.takeRandom(STREET_TYPES)}`,
    postalCode: utils.takeRandom(POSTAL_CODES),
    countryCode: "USA",
    latitude: utils.random(lat1, lat2),
    longitude: utils.random(lng1, lng2),
    createdOn: utils.randomDate(Date.parse("2017"), 180),
    ...values
  };
}

function makePhoneNumber() {
  const parts = ["555", utils.randomInt(222, 888), utils.randomInt(1000, 9999)];
  return parts.join("-");
}

function writeJSON(filename, object) {
  fs.writeFileSync(
    __dirname + "/../data/" + filename,
    JSON.stringify(object, null, 2)
  );
}
