const { send, json } = require("micro");
const models = require("../models");

module.exports = {
  async getAddresses(req, res) {
    send(res, 200, {
      addresses: await models.addresses.findAll(),
      territories: await models.territories.findAll()
    });
  },

  async getAddressesByTerritory(req, res) {
    send(res, 200, {
      addresses: await models.addresses.getByTerritory(req.params.id),
      territory: await models.territories.findByPk(req.params.id)
    });
  },

  async getAddress(req, res) {
    const address = await models.addresses.findByPk(req.params.id);
    if (address) {
      const territory = await models.territories.findByPk(address.territoryId);
      send(res, 200, { address, territory });
    } else {
      send(res, 404, null);
    }
  },

  async getAddressStats(req, res) {
    send(res, 200, await models.addresses.getStats());
  },

  async getAddressHistory(req, res) {
    send(res, 200, {
      history: await models.addressHistory.getByAddress(req.params.id)
    });
  },

  async saveAddress(req, res) {
    const address = await models.addresses.upsert(await json(req));
    const territory = models.territories.findByPk(address.territoryId);
    send(res, 200, { address, territory });
  }
};
