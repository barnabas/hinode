const { send, json } = require("micro");
const models = require("../models");
const STATUS_CODES = require("./status-codes");

module.exports = {
  async getBoundaries(req, res) {
    send(res, STATUS_CODES.OK, {
      boundaries: await models.boundaries.findAll()
    });
  },

  async getBoundary(req, res) {
    const boundary = await models.boundaries.findByPk(req.params.id);
    if (boundary) {
      send(res, STATUS_CODES.OK, { boundary });
    } else {
      send(res, STATUS_CODES.NOT_FOUND, null);
    }
  },

  async saveBoundary(req, res) {
    const boundary = await models.boundaries.upsert(await json(req));
    send(res, boundary ? STATUS_CODES.OK : STATUS_CODES.ERROR, { boundary });
  }
};
