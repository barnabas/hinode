const { send, json } = require("micro");
const models = require("../models");
const STATUS_CODES = require("./status-codes");

module.exports = {
  async getTerritories(req, res) {
    send(res, STATUS_CODES.OK, {
      territories: await models.territories.findAll()
    });
  },

  async getTerritory(req, res) {
    const territory = await models.territories.findByPk(req.params.id);
    if (territory) {
      send(res, STATUS_CODES.OK, { territory });
    } else {
      send(res, STATUS_CODES.NOT_FOUND, null);
    }
  },

  async getTerritoryStats(req, res) {
    send(res, STATUS_CODES.OK, await models.territories.getStats());
  },

  async getTerritoryHistory(req, res) {
    send(res, 200, {
      history: await models.territoryHistory.getByTerritory(req.params.id)
    });
  },

  async saveTerritory(req, res) {
    const territory = await models.territories.upsert(await json(req));
    send(res, territory ? STATUS_CODES.OK : STATUS_CODES.ERROR, { territory });
  }
};
