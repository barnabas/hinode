const data = require("../data/addressHistory.json");

module.exports = {
  getByAddress(id) {
    const filtered = data.history.filter(item => item.addressId === id);
    return Promise.resolve(filtered);
  }
};
