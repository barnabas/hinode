const { upsertItem, incrementKey, takeRandom } = require("../lib/utils");
const data = require("../data/addresses.json");

module.exports = {
  findAll() {
    return Promise.resolve(data.addresses);
  },
  findByPk(id) {
    return Promise.resolve(data.addresses.find(item => item.id === id));
  },
  async upsert(values) {
    const edited = upsertItem(values, data.addresses);
    if (edited.createdOn === edited.modifiedOn) {
      Object.assign(edited, await this.geocode(edited));
    }
    return Promise.resolve(edited);
  },
  geocode(address) {
    // simulate geocoding and assignment: copy lat/lng/territory of random one
    const existing = takeRandom(data.addresses);
    const { latitude, longitude, territoryId } = existing;
    return Promise.resolve({ latitude, longitude, territoryId });
  },
  getByTerritory(id) {
    const filtered = data.addresses.filter(item => item.territoryId === id);
    return Promise.resolve(filtered);
  },
  async getStats(id) {
    const values = id ? await this.getByTerritory(id) : await this.findAll();
    const output = {
      count: values.length,
      statuses: {
        new: 0,
        valid: 0,
        moved: 0,
        duplicate: 0,
        notValid: 0,
        dnc: 0
      }
    };

    values.forEach(v => {
      if (v.status) incrementKey(output.statuses, v.status);
    });

    return output;
  }
};
