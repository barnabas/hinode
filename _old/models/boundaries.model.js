const { upsertItem } = require("../lib/utils");

const data = require("../data/boundaries.json");

module.exports = {
  findAll() {
    return Promise.resolve(data.boundaries);
  },
  findByPk(id) {
    return Promise.resolve(data.boundaries.find(item => item.id === id));
  },
  upsert(values) {
    return Promise.resolve(upsertItem(values, data.boundaries));
  }
};
