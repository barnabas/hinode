const addresses = require("./addresses.model");
const addressHistory = require("./address-history.model");
const boundaries = require("./boundaries.model");
const territories = require("./territories.model");
const territoryHistory = require("./territory-history.model");

module.exports = {
  addresses,
  addressHistory,
  boundaries,
  territories,
  territoryHistory
};
