const { upsertItem, incrementKey } = require("../lib/utils");

const data = require("../data/territories.json");

module.exports = {
  findAll() {
    return Promise.resolve(data.territories);
  },
  findByPk(id) {
    return Promise.resolve(data.territories.find(item => item.id === id));
  },
  upsert(values) {
    return Promise.resolve(upsertItem(values, data.territories));
  },
  getStats() {
    const values = data.territories;
    const output = { count: values.length, statuses: {}, types: {} };

    values.forEach(v => {
      if (v.status) incrementKey(output.statuses, v.status);
      if (v.type) incrementKey(output.types, v.type);
    });

    return Promise.resolve(output);
  }
};
