const data = require("../data/territoryHistory.json");

module.exports = {
  getByTerritory(id) {
    const filtered = data.history.filter(item => item.territoryId === id);
    return Promise.resolve(filtered);
  }
};
