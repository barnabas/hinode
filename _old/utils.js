const MS_PER_DAY = 1000 * 60 * 60 * 24;

module.exports = {
  upsertItem(item, list) {
    if (!item || !item.id || !Array.isArray(list)) return false;
    const idx = list.findIndex(i => i.id === item.id);
    item.modifiedOn = new Date();

    if (idx > -1) {
      list[idx] = item;
    } else {
      item.createdOn = item.modifiedOn;
      list.push(item);
    }
    return item;
  },

  incrementKey(obj, key) {
    if (obj[key] === undefined) obj[key] = 0;
    obj[key] += 1;
  },

  pushKey(obj, key, value) {
    if (obj[key] === undefined) obj[key] = [];
    obj[key].push(value);
  },

  takeRandom(list) {
    return list[Math.floor(Math.random() * list.length)];
  },

  random(n1, n2) {
    return Math.random() * Math.abs(n1 - n2) + Math.min(n1, n2);
  },

  randomInt(i1, i2) {
    return Math.round(random(i1, i2));
  },

  randomDate(last, days = 30) {
    const end = last || Date.now();
    const start = end - MS_PER_DAY * days;
    return new Date(randomInt(start, end));
  }
};
