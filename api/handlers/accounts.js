const { send, createError, json } = require('micro')
const service = require('../services/account.service')
const STATUS_CODES = require('./status-codes')

module.exports = {
  async getAccounts (req, res) {
    const { count, rows: accounts } = await service.getAccounts(req.query)
    send(res, STATUS_CODES.OK, { count, accounts })
  },

  async getAccount (req, res) {
    const account = await service.getAccount(req.params.id)
    if (!account) throw createError(STATUS_CODES.NOT_FOUND)
    return { account }
  },

  async saveAccount (req, res) {
    const data = await json(req)
    let id = req.params.id
    if (id) {
      const count = await service.updateAccount(id, data)
      if (count !== 1) throw createError(STATUS_CODES.ERROR)
    } else {
      id = await service.createAccount(data)
    }
    send(res, STATUS_CODES.OK, { id })
  }
}
