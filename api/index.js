require('dotenv').config()

const { send } = require('micro')
const { router, withNamespace, get, post, put } = require('microrouter')
const accounts = require('./handlers/accounts')
// const addresses = require("./handlers/addresses");
// const boundaries = require("./handlers/boundaries");
// const territories = require("./handlers/territories");

const notfound = (req, res) => send(res, 404, 'Not found')

module.exports = router(
  withNamespace('/api')(
    get('/accounts', accounts.getAccounts),
    post('/accounts', accounts.saveAccount),
    get('/accounts/:id', accounts.getAccount),
    put('/accounts/:id', accounts.saveAccount)

    // get("/addresses", addresses.getAddresses),
    // post("/addresses", addresses.saveAddress),
    // get("/addresses/stats", addresses.getAddressStats),
    // get("/addresses/:id/history", addresses.getAddressHistory),
    // get("/addresses/:id", addresses.getAddress),
    //
    // get("/boundaries", boundaries.getBoundaries),
    // get("/boundaries/:id", boundaries.getBoundary),
    // post("/boundaries", boundaries.saveBoundary),
    //
    // get("/territories", territories.getTerritories),
    // post("/territories", territories.saveTerritory),
    // get("/territories/stats", territories.getTerritoryStats),
    // get("/territories/:id/addresses", addresses.getAddressesByTerritory),
    // get("/territories/:id/history", territories.getTerritoryHistory),
    // get("/territories/:id", territories.getTerritory)
  ),
  get('/*', notfound)
)
