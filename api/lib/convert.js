const camelCase = require('camel-case')
const snakeCase = require('snake-case')

// cache for conversions but could also put special cases here
const SQL_FIELDS = { id: 'id' }
const JSON_FIELDS = { id: 'id' }

function transform (source, fieldMethod) {
  if (source === null || source === undefined) return null
  const output = {}
  Object.entries(source).forEach(([key, val]) => {
    output[fieldMethod(key)] = val
  })
  return output
}

function sqlToJsonField (field) {
  if (!SQL_FIELDS[field]) SQL_FIELDS[field] = camelCase(field)
  return SQL_FIELDS[field]
}

function jsonToSqlField (field) {
  if (!JSON_FIELDS[field]) JSON_FIELDS[field] = snakeCase(field)
  return JSON_FIELDS[field]
}

module.exports = {
  sqlToJson: obj => transform(obj, sqlToJsonField),
  jsonToSql: obj => transform(obj, jsonToSqlField)
}
