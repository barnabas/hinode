const pgp = require('pg-promise')({
  // see: http://vitaly-t.github.io/pg-promise/module-pg-promise.html
})

const db = pgp(process.env.DATABASE_URL)
db.one('SELECT version();').then(console.log)

module.exports = db
