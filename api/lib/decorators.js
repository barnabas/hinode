const jwt = require('jsonwebtoken')
const url = require('url')

const compose = (...fns) => fns.reduce((f, g) => (...args) => f(g(...args)))

function jwtAuth (fn) {
  return (req, res) => {
    const authHead = req.headers.authorization || ''

    try {
      const token = authHead.substr(authHead.indexOf(' ') + 1)
      if (!token && req.connection.localAddress === req.connection.remoteAddress) {
        // allow fake user for localhost testing
        req.user = { id: '00000000-0000-0000-0000-000000000001', username: 'system' }
      } else {
        req.user = jwt.verify(token, process.env.JWT_SECRET)
      }
    } catch (err) {
      err.statusCode = 401
      throw err
    }

    return fn(req, res)
  }
}

function nocache (fn) {
  return (req, res) => {
    res.setHeader('Surrogate-Control', 'no-store')
    res.setHeader('Cache-Control', 'no-store, no-cache, must-revalidate, proxy-revalidate')
    res.setHeader('Pragma', 'no-cache')
    res.setHeader('Expires', '0')
    return fn(req, res)
  }
}

function secureHeaders (fn) {
  return (req, res) => {
    res.setHeader('X-Frame-Options', 'sameorigin') // helmet frameguard
    res.setHeader('Strict-Transport-Security', 'max-age=5184000') // helmet HSTS
    res.setHeader('X-XSS-Protection', '1; mode=block') // helmet xss-filter
    return fn(req, res)
  }
}

function logger (fn) {
  return (req, res) => {
    const startTime = process.hrtime()
    const startSize = req.socket.bytesWritten
    res.on('finish', () => {
      const reqUrl = url.parse(req.url, true)
      const diff = process.hrtime(startTime)
      const msg = {
        type: 'log',
        data: {
          url: reqUrl.pathname,
          query: reqUrl.query,
          method: req.method,
          responseTime: +(diff[0] * 1e3 + diff[1] * 1e-6).toFixed(3),
          size: req.socket.bytesWritten - startSize,
          user: req.user ? req.user.id : 'unknown'
        }
      }
      process.send(msg)
    })
    return fn(req, res)
  }
}

module.exports = {
  jwtAuth,
  nocache,
  secureHeaders,
  logger,
  open: compose(
    logger,
    secureHeaders,
    nocache
  ),
  all: compose(
    jwtAuth,
    logger,
    secureHeaders,
    nocache
  )
}
