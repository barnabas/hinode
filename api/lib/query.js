const db = require('./db')
const { sqlToJson, jsonToSql } = require('./convert')

function getCountSQL (sources, conditions) {
  const sql = [`SELECT count(*)`, `FROM ${sources.join('\nINNER JOIN')}`]
  if (conditions && conditions.length > 0)
    sql.push('WHERE ' + conditions.join(' AND '))
  return sql.join('\n')
}

function getFullSQL (sources, conditions, params) {
  const sql = [`SELECT $(fields:name)`, `FROM ${sources.join('\nINNER JOIN')}`]
  if (conditions && conditions.length > 0)
    sql.push('WHERE ' + conditions.join(' AND '))

  if (params.sortBy) {
    sql.push('ORDER BY $(sortBy:name) ' + (params.descending ? 'DESC' : 'ASC'))
  }

  return sql.join('\n')
}

function getPagination (params) {
  const size = parseInt(params.size, 10)
  const page = parseInt(params.page, 10)
  const pagination = {
    start: 0,
    size: isNaN(size) || size < 1 ? 0 : size,
    page: isNaN(page) || page < 1 ? 1 : page
  }
  pagination.start = Math.max(pagination.page - 1, 0) * pagination.size
  return pagination
}

function getCountAndRows (fields, sources, conditions, params) {
  const pagination = getPagination(params)
  let limitClause = ''

  if (pagination.size > 0) {
    limitClause = ` LIMIT ${pagination.size} OFFSET ${pagination.start}`
  }

  const countSQL = getCountSQL(sources, conditions)
  const fullSQL = getFullSQL(sources, conditions, params) + limitClause

  return db.task(async t => {
    const count = await t.one(countSQL, params, r => parseInt(r.count))
    const rows = count > 0 ? await t.many(fullSQL, { fields, ...params }) : []
    return { count, pagination, rows: rows.map(sqlToJson) }
  })
}

function insertRow (table, properties) {
  const props = jsonToSql(properties)
  const keys1 = Object.keys(props)
  const keys2 = keys1.map(k => `$(${k})`)
  return db.one(
    `INSERT INTO $(table:name)(${keys1.join(',')}) VALUES(${keys2.join(',')}) RETURNING id`,
    { ...props, table },
    r => r.id
  )
}

function updateRow (table, id, properties) {
  const props = jsonToSql(properties)
  const statements = Object.keys(props).map(k => `${k} = $(${k})`)
  return db.result(
    `UPDATE $(table:name) SET ${statements.join(', ')} WHERE id = $(id)`,
    { ...props, table, id },
    r => r.rowCount
  )
}

module.exports = {
  getCountAndRows,
  insertRow,
  updateRow
}
