const db = require('../lib/db')
const { sqlToJson } = require('../lib/convert')
const { getCountAndRows, insertRow, updateRow } = require('../lib/query')

const FIELDS = ['id', 'name', 'country_code', 'region_code', 'language_code']

async function getAccount (accountId) {
  const row = await db.oneOrNone(
    `SELECT $(fields:name) FROM accounts WHERE id = $(accountId)`,
    { accountId, fields: FIELDS }
  )
  return sqlToJson(row)
}

function getAccounts (parameters) {
  const conditions = []
  const sources = ['accounts']
  const params = parameters || {}

  if (params.userId !== undefined) {
    conditions.push('au.user_id = $(userId)')
    sources.push('accounts_users AS au ON accounts.id = au.account_id')
  }

  return getCountAndRows(FIELDS, sources, conditions, params)
}

module.exports = {
  getAccount,
  getAccounts,
  getAccountsByUser: (userId, parameters) => getAccounts({ userId, ...parameters }),
  createAccount: properties => insertRow('accounts', properties),
  updateAccount: (accountId, properties) => updateRow('accounts', accountId, properties)
}
