const db = require('../lib/db')
const { sqlToJson } = require('../lib/convert')
const { getCountAndRows, insertRow, updateRow } = require('../lib/query')

const FIELDS = ['id', 'login', 'name', 'email', 'mobile', 'active', 'permissions']

async function getUser (userId) {
  const row = await db.oneOrNone(
    'SELECT ${FIELDS:name} FROM users WHERE id = $(userId)',
    { userId, FIELDS }
  )
  return sqlToJson(row)
}

async function loginUser (login, password) {
  const row = await db.oneOrNone(
    'SELECT ${FIELDS:name} FROM users WHERE login = ${login} AND active = true AND pwhash = crypt(${password}, pwhash)',
    { login, password, FIELDS }
  )
  return sqlToJson(row)
}

const updatePassword = (userId, password) => db.result(
  'UPDATE users SET pwhash = crypt(${password}, gen_salt(\'bf\')) WHERE id = ${userId}',
  { userId, password },
  r => r.rowCount
)

function getUsers (parameters) {
  const conditions = []
  const sources = ['users']
  const params = parameters || {}

  if (params.accountId !== undefined) {
    conditions.push('au.account_id = ${accountId}')
    sources.push('accounts_users AS au ON users.id = au.user_id')
  }

  return getCountAndRows(FIELDS, sources, conditions, params)
}

const addUserAccount = (userId, accountId) => insertRow('accounts_users', { userId, accountId })

const removeUserAccount = (userId, accountId) => db.result(
  'DELETE accounts_users WHERE user_id = ${userId} AND account_id = ${accountId}',
  { userId, accountId },
  r => r.rowCount
)

module.exports = {
  getUser,
  getUsers,
  loginUser,
  updatePassword,
  getUsersByAccount: (accountId, parameters) => getUsers({ accountId, ...parameters }),
  createUser: properties => insertRow('users', properties),
  updateUser: (userId, properties) => updateRow('users', userId, properties),
  addUserAccount,
  removeUserAccount
}
