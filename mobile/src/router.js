import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Territory from "./views/Territory.vue";

Vue.use(Router);

export default new Router({
  routes: [
    { path: "/", name: "home", component: Home },
    {
      path: "/:id",
      name: "territory",
      component: Territory,
      props: true
    }
  ]
});
