module.exports = {
  lintOnSave: false,
  publicPath: process.env.NODE_ENV === "production" ? "/mobile/" : "/",
  // chainWebpack: config => config.resolve.symlinks(false),
  devServer: {
    proxy: {
      "^/api": {
        target: "http://localhost:3000"
      }
    }
  }
};
