import { parseDate } from "./model-utils";
import uuid from "uuid/v4";

export class BaseModel {
  constructor(data) {
    const d = data || {};

    // multi-tenant account ID unnecessary; handled transparently by API and authorization
    this.id = d.id || uuid();
    this.name = d.name;
    this.createdOn = parseDate(d.createdOn);
    this.modifiedOn = parseDate(d.modifiedOn);
  }

  toJSON() {
    return { id: this.id, name: this.name };
  }

  makeCopy() {
    // ex: return new BaseModel(this.toJSON())
    throw new Error("Override makeCopy");
  }
}
