import { parseDate } from "./model-utils";
import uuid from "uuid/v4";

export class HistoryModel {
  constructor(data) {
    const d = data || {};

    this.id = d.id || uuid();
    this.status = d.status;
    this.createdOn = parseDate(d.createdOn);
  }

  toJSON() {
    return { id: this.id, status: this.status };
  }

  makeCopy() {
    // ex: return new BaseModel(this.toJSON())
    throw new Error("Override makeCopy");
  }
}
