import { HistoryModel } from "./_history.model";

export class AddressHistoryModel extends HistoryModel {
  constructor(data) {
    super(data);
    const d = data || {};
    this.note = d.note;
  }

  toJSON() {
    return {
      ...super.toJSON(),
      note: this.note
    };
  }
}
