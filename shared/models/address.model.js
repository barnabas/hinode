import { BaseModel } from "./_base.model";
import { TerritoryModel } from "./territory.model";
import { filterMatch, makeRequiredRule } from "./model-utils";
import { tryCreateModel } from "../services/service-utils";
import { STATUS_ORDER } from "../types";

export class AddressModel extends BaseModel {
  constructor(data) {
    super(data);
    const d = data || {};
    this.status = d.status || "new";
    this.phoneNumber = d.phoneNumber;
    this.streetAddress = d.streetAddress; // further breakdown unnecessary (apartment, multilines)
    this.city = d.city;
    this.postalCode = d.postalCode;
    // country and state/province unnecessary; set at account level
    this.latitude = d.latitude;
    this.longitude = d.longitude;
    this.notes = d.notes;
    this.territoryId = d.territoryId;

    this.territory = null;
    if (d.territory) {
      this.territory = tryCreateModel(d.territory, TerritoryModel);
    }
  }

  get coordinates() {
    return [this.longitude, this.latitude];
  }

  set coordinates(value) {
    const [longitude, latitude] = Array.isArray(value) ? value : [];
    Object.assign(this, { longitude, latitude });
  }

  get fullAddress() {
    return [this.streetAddress, this.city, this.postalCode].join(", ");
  }

  get statusOrder() {
    return STATUS_ORDER[this.status] || 99;
  }

  toGeoJSON() {
    return Object.freeze({
      type: "Feature",
      properties: {
        id: this.id,
        name: this.name,
        phoneNumber: this.phoneNumber,
        fullAddress: this.fullAddress,
        status: this.status
      },
      geometry: { type: "Point", coordinates: this.coordinates }
    });
  }

  toJSON() {
    return {
      ...super.toJSON(),
      status: this.status,
      phoneNumber: this.phoneNumber,
      streetAddress: this.streetAddress,
      city: this.city,
      postalCode: this.postalCode,
      latitude: this.latitude,
      longitude: this.longitude,
      notes: this.notes,
      territoryId: this.territoryId
    };
  }

  makeCopy() {
    return new AddressModel(this.toJSON());
  }

  matches(filter) {
    if (filter === null) return true;
    if (!filterMatch(filter.status, this.status)) return false;
    if (!filterMatch(filter.statuses, this.status)) return false;

    if (filter.query)
      return [this.name, this.streetAddress, this.city, this.postalCode]
        .join(" ")
        .toLowerCase()
        .includes(filter.query.toLowerCase());
    return true;
  }
}

export function getAddressRules(t) {
  return {
    name: [makeRequiredRule(t, "name")],
    streetAddress: [makeRequiredRule(t, "streetAddress")],
    city: [makeRequiredRule(t, "city")],
    postalCode: [makeRequiredRule(t, "postalCode")]
  };
}
