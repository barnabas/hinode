import { BaseModel } from "./_base.model";

export class BoundaryModel extends BaseModel {
  constructor(data) {
    super(data);
    const d = data || {};
    this.geometry = d.geometry || { type: "Polygon", coordinates: [] };
  }

  toJSON() {
    return {
      ...super.toJSON(),
      geometry: this.geometry
    };
  }

  toGeoJSON() {
    return Object.freeze({
      type: "Feature",
      properties: { id: this.id, name: this.name },
      geometry: this.geometry
    });
  }

  makeCopy() {
    return new BoundaryModel(this.toJSON());
  }
}
