export function parseDate(value) {
  if (value === null || value === undefined) return null;
  if (value instanceof Date) return value;
  return new Date(value);
}

export function makeRequiredRule(t, field) {
  return v => !!v || t("errors.fieldRequired", { field: t("fields." + field) });
}

export function filterMatch(filterValue, itemValue) {
  if (filterValue === undefined || filterValue === null) return true;

  if (Array.isArray(filterValue)) {
    return filterValue.length < 1 || filterValue.includes(itemValue);
  } else {
    return filterValue === itemValue;
  }
}
