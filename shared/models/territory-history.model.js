import { HistoryModel } from "./_history.model";

export class TerritoryHistoryModel extends HistoryModel {
  constructor(data) {
    super(data);
    const d = data || {};
    this.userId = d.userId;
  }

  toJSON() {
    return {
      ...super.toJSON(),
      userId: this.userId
    };
  }
}
