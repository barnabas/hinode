import { BaseModel } from "./_base.model";
import { STATUS_ORDER } from "../types";
import { makeRequiredRule } from "./model-utils";

export class TerritoryModel extends BaseModel {
  constructor(data) {
    super(data);
    const d = data || {};
    this.type = d.type || null;
    this.status = d.status || null;
  }

  get statusOrder() {
    return STATUS_ORDER[this.status] || 99;
  }

  toJSON() {
    return {
      ...super.toJSON(),
      type: this.type,
      status: this.status
    };
  }

  matches(filter) {
    if (!filter) return true;
    if (filter.status && this.status !== filter.status) return false;
    return true;
  }

  makeCopy() {
    return new TerritoryModel(this.toJSON());
  }
}

export function getTerritoryRules(t) {
  return {
    name: [makeRequiredRule(t, "name")],
    type: [makeRequiredRule(t, "type")],
    status: [makeRequiredRule(t, "status")]
  };
}
