import axios from "axios";
import { AddressModel } from "../models/address.model";
import { TerritoryModel } from "../models/territory.model";
import { AddressHistoryModel } from "../models/address-history.model";
import * as utils from "./service-utils";

export async function getAddresses() {
  const { data } = await axios.get("/api/addresses");
  const territories = utils.tryCreateModelMap(data.territories, TerritoryModel);
  const addresses = utils.tryCreateModelArray(data.addresses, AddressModel);
  addresses.forEach(item => (item.territory = territories[item.territoryId]));
  return addresses;
}

function populateAddress(response) {
  const { data } = response;
  const address = new AddressModel(data.address);
  address.territory = utils.tryCreateModel(data.territory, TerritoryModel);
  return address;
}

export async function getAddress(id) {
  return populateAddress(await axios.get(`/api/addresses/${id}`));
}

export async function getTerritoryAddresses(id) {
  const { data } = await axios.get(`/api/territories/${id}/addresses`);
  return utils.tryCreateModelArray(data.addresses, AddressModel);
}

export async function getAddressHistory(id) {
  const { data } = await axios.get(`/api/addresses/${id}/history`);
  return utils.tryCreateModelArray(data.history, AddressHistoryModel);
}

export async function saveAddress(address) {
  return populateAddress(await axios.post(`/api/addresses`, address.toJSON()));
}

export async function getAddressStats() {
  const { data } = await axios.get("/api/addresses/stats");
  return data;
}
