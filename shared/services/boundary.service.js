import axios from "axios";
import { BoundaryModel } from "../models/boundary.model";
import { tryCreateModel, tryCreateModelArray } from "./service-utils";

export async function getBoundaries() {
  const { data } = await axios.get("/api/boundaries");
  return tryCreateModelArray(data.boundaries, BoundaryModel);
}

export async function getBoundary(id) {
  const { data } = await axios.get(`/api/boundaries/${id}`);
  return tryCreateModel(data.boundary, BoundaryModel);
}

export async function saveBoundary(boundary) {
  const { data } = await axios.post(`/api/boundaries`, boundary.toJSON());
  return tryCreateModel(data.boundary, BoundaryModel);
}
