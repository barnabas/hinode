export function tryCreateModel(object, model) {
  if (object === null || object === undefined) return null;
  return object instanceof model ? object : new model(object);
}

export function tryCreateModelArray(array, model) {
  return Array.isArray(array)
    ? array.map(obj => tryCreateModel(obj, model))
    : [];
}

export function tryCreateModelMap(array, model) {
  const map = {};
  tryCreateModelArray(array, model).forEach(item => {
    map[item.id] = item;
  });
  return map;
}

export function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
