import axios from "axios";
import { TerritoryModel } from "../models/territory.model";
import { TerritoryHistoryModel } from "../models/territory-history.model";
import { tryCreateModel, tryCreateModelArray } from "./service-utils";

export async function getTerritories() {
  const { data } = await axios.get("/api/territories");
  return tryCreateModelArray(data.territories, TerritoryModel);
}

export async function getTerritory(id) {
  const { data } = await axios.get(`/api/territories/${id}`);
  return tryCreateModel(data.territory, TerritoryModel);
}

export async function getTerritoryHistory(id) {
  const { data } = await axios.get(`/api/territories/${id}/history`);
  return tryCreateModelArray(data.history, TerritoryHistoryModel);
}

export async function saveTerritory(territory) {
  const { data } = await axios.post(`/api/territories`, territory.toJSON());
  return tryCreateModel(data.territory, TerritoryModel);
}

export async function getTerritoryStats() {
  const { data } = await axios.get("/api/territories/stats");
  return data;
}
