export const ADDRESS_STATUSES = [
  "new",
  "valid",
  "moved",
  "duplicate",
  "notValid",
  "dnc"
];

export const TERRITORY_STATUSES = ["available", "signedOut"];

export const TERRITORY_TYPES = ["standard", "telephone", "business"];

export const STATUS_ORDER = {
  available: 1,
  signedOut: 2,

  new: 1,
  valid: 2,
  moved: 3,
  duplicate: 4,
  notValid: 5,
  dnc: 6
};
