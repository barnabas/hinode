import Vue from "vue";
import "./plugins/vuetify";
import i18n from "./i18n.js";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import VueLayers from "vuelayers";
import "vuelayers/lib/style.css"; // needs css-loader

Vue.use(VueLayers);
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount("#app");
