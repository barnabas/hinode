import Vue from "vue";
import Vuetify from "vuetify/lib";
import { Resize } from "vuetify/lib/directives";
import colors from "vuetify/es5/util/colors";

import "../main.styl";
import "@fortawesome/fontawesome-free/css/solid.min.css";
import "@fortawesome/fontawesome-free/css/regular.min.css";
import "@fortawesome/fontawesome-free/css/fontawesome.min.css";

Vue.use(Vuetify, {
  iconfont: "fa",
  directives: { Resize },
  theme: {
    primary: colors.green.darken2
  }
});
