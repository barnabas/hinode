import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Addresses from "./views/Addresses.vue";
import Address from "./views/Address.vue";
import Territories from "./views/Territories.vue";
import Territory from "./views/Territory.vue";
import Admin from "./views/Admin.vue";
import About from "./views/About.vue";
import Login from "./views/Login.vue";
import store from "./store";

Vue.use(Router);

const router = new Router({
  routes: [
    { path: "/", name: "home", component: Home },
    { path: "/addresses", name: "addresses", component: Addresses },
    {
      path: "/addresses/:id",
      name: "address",
      component: Address,
      props: true
    },
    { path: "/territories", name: "territories", component: Territories },
    {
      path: "/territories/:id",
      name: "territory",
      component: Territory,
      props: true
    },
    { path: "/admin", name: "admin", component: Admin },
    { path: "/about", name: "about", component: About },
    { path: "/login", name: "login", meta: { hideNav: true }, component: Login }
  ]
});

router.beforeEach((to, from, next) => {
  if (to.name !== "login" && !store.getters.isLoggedIn) {
    return next({ name: "login", replace: true });
  }
  next();
});

export default router;
