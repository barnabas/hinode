import axios from "axios";

export async function loginUser(user) {
  const { data } = await axios.post("/auth/login", user);
  return data;
}

export async function resetPassword(user) {
  const { data } = await axios.post("/auth/reset", user);
  return data;
}
