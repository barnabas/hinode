import Vue from "vue";
import Vuex from "vuex";

import { BoundaryModel } from "../../shared/models/boundary.model";
import { TerritoryModel } from "../../shared/models/territory.model";
import { AddressModel } from "../../shared/models/address.model";
import * as territorySvc from "../../shared/services/territory.service";
import * as boundarySvc from "../../shared/services/boundary.service";
import * as addressSvc from "../../shared/services/address.service";

Vue.use(Vuex);

const storage = {
  USER: "user",
  ACCOUNT: "account",
  getObject(key, defaultValue) {
    const data = localStorage.getItem(key);
    return data === null ? defaultValue : JSON.parse(data);
  },
  setObject(key, value) {
    if (!value) {
      localStorage.removeItem(key);
    } else {
      localStorage.setItem(key, JSON.stringify(value));
    }
  }
};

export default new Vuex.Store({
  state: {
    account: storage.getObject(storage.ACCOUNT, {}),
    alert: { show: false, color: "info", message: "" },
    appRect: {},
    address: new AddressModel(),
    addresses: [],
    boundaries: [],
    boundary: new BoundaryModel(),
    territories: [],
    territory: new TerritoryModel(),
    user: storage.getObject(storage.USER, {})
  },
  mutations: {
    setAccount(state, payload) {
      storage.setObject(storage.ACCOUNT, payload);
      state.account = payload || {};
    },
    setAlert(state, payload) {
      state.alert = payload;
    },
    setAppRect(state, payload) {
      state.appRect = payload;
    },
    setAddress(state, payload) {
      state.address = payload;
    },
    setAddresses(state, payload) {
      state.addresses = payload;
    },
    setBoundary(state, payload) {
      state.boundary = payload;
    },
    setBoundaries(state, payload) {
      state.boundaries = payload;
    },
    setTerritory(state, payload) {
      state.territory = payload;
    },
    setTerritories(state, payload) {
      state.territories = payload;
    },
    setUser(state, payload) {
      storage.setObject(storage.USER, payload);
      state.user = payload || {};
    }
  },
  actions: {
    async loadTerritories({ commit }) {
      commit("setTerritories", await territorySvc.getTerritories());
    },
    async loadTerritory({ commit }, id) {
      commit("setTerritory", await territorySvc.getTerritory(id));
    },
    async loadBoundaries({ commit }) {
      commit("setBoundaries", await boundarySvc.getBoundaries());
    },
    async loadBoundary({ commit }, id) {
      commit("setBoundary", await boundarySvc.getBoundary(id));
    },
    async loadAddresses({ commit }) {
      commit("setAddresses", await addressSvc.getAddresses());
    },
    async loadAddress({ commit }, id) {
      commit("setAddress", await addressSvc.getAddress(id));
    }
  },
  getters: {
    isLoggedIn: state => !!state.account.token
  }
});
