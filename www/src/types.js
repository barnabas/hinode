export function getValueList(types, t) {
  if (!Array.isArray(types)) return [];
  return types.map(value => ({ value, text: t("values." + value) }));
}

// http://colorbrewer2.org/?type=diverging&scheme=BrBG&n=6
const SCHEME = [
  "#8c510a",
  "#d8b365",
  "#f6e8c3",
  "#c7eae5",
  "#5ab4ac",
  "#01665e"
];
export const STATUS_COLORS = {
  // territory statuses
  available: SCHEME[0],
  signedOut: SCHEME[1],
  // address statuses
  new: SCHEME[0],
  valid: SCHEME[1],
  moved: SCHEME[2],
  duplicate: SCHEME[3],
  notValid: SCHEME[4],
  dnc: SCHEME[5],
  // address types
  standard: SCHEME[0],
  telephone: SCHEME[1],
  business: SCHEME[2]
};

export const STATUS_IS_DARK = {
  available: true,
  new: true,
  dnc: true
};

export const STATUS_ICONS = {
  available: "fas fa-check",
  signedOut: "fas fa-pause",

  new: "fas fa-star",
  valid: "fas fa-check",
  moved: "fas fa-truck-moving",
  duplicate: "fas fa-clone",
  notValid: "fas fa-ban",
  dnc: "fas fa-hand-paper"
};
