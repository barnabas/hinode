import { mapState, mapActions, mapMutations } from "vuex";

export default {
  mounted() {
    const key = "navigation." + this.$route.name;
    this.setTitle(this.$te(key) ? this.$t(key) : null);
  },
  computed: {
    ...mapState([
      "address",
      "addresses",
      "boundary",
      "boundaries",
      "territory",
      "territories"
    ])
  },
  methods: {
    setTitle(...parts) {
      document.title = ["Hinode"]
        .concat(parts)
        .filter(v => !!v)
        .join(" | ");
    },
    showAlertError(err) {
      let { message, response } = err;
      if (response.data && response.data.code) {
        const key = "errors." + response.data.code;
        if (this.$te(key)) message = this.$t(key);
      }
      this.setAlert({ show: true, color: "error", message });
    },
    ...mapMutations(["setAlert"]),
    ...mapActions([
      "loadAddress",
      "loadAddresses",
      "loadBoundary",
      "loadBoundaries",
      "loadTerritory",
      "loadTerritories"
    ])
  }
};
