import { shallowMount } from "@vue/test-utils";
import StaticField from "@/components/StaticField.vue";

describe("StaticField.vue", () => {
  it("renders props.value when passed", () => {
    const value = "new message";
    const wrapper = shallowMount(StaticField, {
      propsData: { value },
      mocks: { $t: () => "" }
    });
    expect(wrapper.text()).toMatch(value);
  });
});
