import { getAddressStats } from "@/services/address.service";

describe("Address Service", () => {
  it("returns something when empty", () => {
    const stats = getAddressStats();
    expect(stats.count).toBe(0);
  });

  it("returns valid stats", () => {
    const data = [
      "new",
      "valid",
      "moved",
      "duplicate",
      "duplicate",
      "notValid",
      "dnc"
    ].map(status => ({ status }));
    const stats = getAddressStats(data);
    expect(stats.count).toBe(7);
    expect(stats.statuses.new).toBe(1);
    expect(stats.statuses.valid).toBe(1);
    expect(stats.statuses.moved).toBe(1);
    expect(stats.statuses.duplicate).toBe(2);
    expect(stats.statuses.notValid).toBe(1);
    expect(stats.statuses.dnc).toBe(1);
  });
});
