module.exports = {
  lintOnSave: false,

  devServer: {
    proxy: {
      "^/api": {
        target: "http://localhost:3000"
      }
    }
  },

  pluginOptions: {
    i18n: {
      locale: "en",
      fallbackLocale: "en",
      localeDir: "locales",
      enableInSFC: false
    }
  }
};
